The script allows to quickly filter out any lines from the file that match a set of regex expressions.

## Usage
Running the script opens a file selection window for picking a file that will be filtered.  
After selecting the file, a seocnd file selection appears for selecting a file containing regex expression named in a `regex_.*.txt` format.  
Another window allows for specifying the encoding of a file that will be processed.  
File gets processed and any line that matches one of the regex expressions specified in the `regex_` file will be filtered out and the output writted to a file with `_filtered_` and a date in `dd-mm-yyyy` format appended to the original name.  

import tkinter as tk
from tkinter import filedialog, ttk, scrolledtext
import os
import re
from datetime import datetime

def select_encoding(title):
    encoding = 'utf-8'
    
    encodings_list = ["utf-8", "oem"];

    def on_selection():
        nonlocal encoding
        encoding = encoding_combobox.get()
        root.destroy()

    root = tk.Tk()
    root.title(title)

    encoding_label = ttk.Label(root, text="Select Encoding:")
    encoding_label.pack()

    # Get the list of available encodings
    available_encodings = sorted(set([enc.lower() for enc in encodings_list]))

    encoding_combobox = ttk.Combobox(root, values=available_encodings, state="readonly")
    encoding_combobox.current(0)
    encoding_combobox.pack()

    encode_button = ttk.Button(root, text="Select", command=on_selection)
    encode_button.pack()

    root.mainloop()

    return encoding
    
def filter_file():
    # Select the file to filter
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(title='Select file to filter')
    root.destroy()
    
    if not file_path:
        exit()

    # Get all files starting with 'regex_' in the current directory
    regex_files = [file for file in os.listdir('.') if file.startswith('regex_')]
    
    # Select the regex file to use for filtering
    root = tk.Tk()
    root.withdraw()
    regex_file_path = filedialog.askopenfilename(filetypes=[('Text Files', '*.txt')], title='Select file with filtering rules (one per line)')
    root.destroy()
    
    if not regex_file_path:
        exit()
    
    selected_encoding = select_encoding("Please select input file encoding")
    
    root = tk.Tk()
    root.title("Replacement progress")
    console_text = scrolledtext.ScrolledText(root, height=15, width=50, wrap=tk.WORD)
    console_text.pack()
    console_lines = 0
    console_max_lines = 100
    removed_lines = 0
    
    def root_update():
        root.update_idletasks()
        root.update()
    
    filtered_file_path = os.path.splitext(file_path)[0] + '_filtered_' + datetime.now().strftime('%d-%m-%Y') + '.txt'
    # Read regex patterns from the regex file
    
    console_text.insert(tk.END, "Processing…" + '\n')
    console_text.yview(tk.END)
    root_update()
    try:
        regex_list = []
        with open(regex_file_path, 'r', encoding='utf-8') as regex_file:
            for filter_pattern in regex_file:
                filter_pattern = filter_pattern.strip()
                if filter_pattern and not filter_pattern.startswith("#"):
                    regex_list.append(filter_pattern)
                    console_text.insert(tk.END, "Loaded pattern '" + filter_pattern + "'…" + '\n')
                    console_text.yview(tk.END)
                    console_lines += 1
                    root_update()
        
        with open(file_path, 'r', encoding=selected_encoding) as file:
            with open(filtered_file_path, 'w', encoding='utf-8') as filtered_file:
                for line in file:
                    remove = False
                    for filter_pattern in regex_list:
                        if re.match(filter_pattern, line):
                            remove = True
                            break
                    if not remove:
                        filtered_file.write(line)
                    else:
                        removed_lines += 1
                        if removed_lines < 100 or removed_lines % 1000 == 0:
                            console_lines += 1
                            if console_lines > console_max_lines:
                                console_text.delete("1.0", f"{console_max_lines}.end")
                                console_lines = 0
                            console_text.insert(tk.END, "Removed lines: " + str(removed_lines) + '\n')
                            console_text.yview(tk.END)
                            root_update()
    except FileNotFoundError as error:
        console_text.insert(tk.END, "Could not open a file: '" + str(error) + "'" + '\n')
                    

    console_text.insert(tk.END, "Finished!" + '\n')
    console_text.yview(tk.END)
    root.protocol('WM_DELETE_WINDOW', root.destroy)
    root.mainloop()

if __name__ == '__main__':
    filter_file()
